package com.inearth.howcanihelpyou;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.firebase.iid.FirebaseInstanceId;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.iid.FirebaseInstanceId;
import com.himanshurawat.hasher.Hasher;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    Button mEmailSignInButton;
    RequestQueue MyRequestQueue;
    Context context;

    private Dialog loader;
    private Dialog errorDialog;
    private Dialog validateDialog;
    private Dialog regiterDialog;

    private TextView btnInvitation;
    private Button btnValidateCode;
    private EditText txtCode;

    private EditText txtFirstname;
    private EditText txtLastname;
    private EditText txtPassword;
    private EditText txtPasswordConfirm;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;

        loader = new Dialog(context);
        loader.setContentView(R.layout.loading);
        loader.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        RotateAnimation rotateAnimation = new RotateAnimation(0, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(500);
        rotateAnimation.setRepeatCount(Animation.INFINITE);

        loader.findViewById(R.id.loadicon).startAnimation(rotateAnimation);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        MyRequestQueue = Volley.newRequestQueue(this);

        // Set up the login form.
        mEmailView = findViewById(R.id.email);
        mPasswordView = findViewById(R.id.password);
        btnInvitation = findViewById(R.id.btnInvitation);

        mEmailView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEmailView.setHint("");
                return false;
            }
        });

        mEmailView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mEmailView.setHint("Email");
                }
            }
        });

        mPasswordView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mPasswordView.setHint("");
                return false;
            }
        });

        mPasswordView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    mPasswordView.setHint("Password");
                }
            }
        });

        btnInvitation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //attemptLogin();
                validateDialog.show();
            }
        });

        mEmailSignInButton = findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        errorDialog = new Dialog(this );
        validateDialog = new Dialog(this );
        regiterDialog = new Dialog(this );

        errorDialog.setContentView(R.layout.error);
        errorDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        validateDialog.setContentView(R.layout.validatecode_layout);
        validateDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        regiterDialog.setContentView(R.layout.register_layout);
        regiterDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        btnValidateCode = validateDialog.findViewById(R.id.btnValidateCode);
        txtCode = validateDialog.findViewById(R.id.code);

        txtFirstname = regiterDialog.findViewById(R.id.firstname);
        txtLastname = regiterDialog.findViewById(R.id.lastname);
        txtPassword = regiterDialog.findViewById(R.id.password);
        txtPasswordConfirm = regiterDialog.findViewById(R.id.passwordConfirm);
        btnRegister = regiterDialog.findViewById(R.id.btnRegister);

        btnValidateCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //attemptLogin();
                validateCode();
            }
        });

        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //attemptLogin();
                registrar();
            }
        });
        
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        
        System.out.println("~> ~> ~> ~> ~> Token: " + refreshedToken);

    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        mEmailSignInButton.setVisibility(View.GONE);
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            mEmailSignInButton.setVisibility(View.VISIBLE);
            focusView.requestFocus();
            return;
        }

        String url = "https://howcanihelpyou.app/mobilelogin";

        loader.show();

        JSONObject jsonBody = new JSONObject();

        try {
            String hash = new Hasher().hash(mPasswordView.getText().toString(), Hasher.MD5).toUpperCase();
//            String token = FirebaseInstanceId.getInstance().getToken();
            String token = FirebaseInstanceId.getInstance().getToken();
            jsonBody.put("email", mEmailView.getText().toString());
            jsonBody.put("password", hash);
            jsonBody.put("token", token);
//            jsonBody.put("token", token);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }


        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loader.dismiss();
                try {
                    if( response.getBoolean("success") ){
                        SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                                "com.inearth.howcanihelpyou", MainActivity.MODE_PRIVATE);
                        prefs.edit().putString("session", response.getString("message")).apply();

                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("session",response.getString("message"));
                        editor.commit();
                        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                        getApplicationContext().startActivity(myIntent);

                    } else {
                        TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
                        int idwrongcrenditlas = getResources().getIdentifier ("wrong_credentials","string",context.getPackageName());

                        txtErrorDialog.setText( getString( idwrongcrenditlas ) );
                        Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
                        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errorDialog.dismiss();
                            }
                        });
                        errorDialog.show();
                        mEmailSignInButton.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    mEmailSignInButton.setVisibility(View.VISIBLE);
                }

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                loader.dismiss();

                TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
                int idwrongcrenditlas = getResources().getIdentifier ("wrong_credentials","string",context.getPackageName());
                        //context.getResources().getIdentifier("wrong_credentials", "strings", context.getPackageName());

                txtErrorDialog.setText( getString( idwrongcrenditlas ) );
                Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
                btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        errorDialog.dismiss();

                    }
                });
                errorDialog.show();

                mEmailSignInButton.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        } ;

        jsonOblect.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(jsonOblect);
    }

    private void validateCode() {

        String url = "https://howcanihelpyou.app/isValidCode";

        loader.show();

        JSONObject jsonBody = new JSONObject();

        try {
            String hash = new Hasher().hash(mPasswordView.getText().toString(), Hasher.MD5).toUpperCase();
            jsonBody.put("code", txtCode.getText().toString().toUpperCase() );
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loader.dismiss();

                try {

                    if (response.getBoolean("success") ) {

                        txtFirstname.setText("");
                        txtLastname.setText("");
                        txtPassword.setText("");
                        txtPasswordConfirm.setText("");

                        regiterDialog.show();

                    } else {

                        TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
                        int idwrongcrenditlas = getResources().getIdentifier ("invalid_code","string",context.getPackageName());

                        txtErrorDialog.setText( getString( idwrongcrenditlas ) );
                        Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);

                        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errorDialog.dismiss();
                            }
                        });

                        errorDialog.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loader.dismiss();


            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }

        } ;

        jsonOblect.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(jsonOblect);
    }


    public void registrar() {
        View focusView = null;

        if( !isStringNullOrWhiteSpace(txtFirstname.getText().toString()) ||
                !isStringNullOrWhiteSpace(txtLastname.getText().toString()) ||
                !isStringNullOrWhiteSpace(txtPassword.getText().toString()) ||
                !isStringNullOrWhiteSpace(txtPasswordConfirm.getText().toString()) ) {

            if (!isPasswordValid(txtPassword.getText().toString())) {

                txtPassword.setError(getString(R.string.error_invalid_password));

            } else if( !txtPassword.getText().toString().equals(txtPasswordConfirm.getText().toString())  ) {

                txtPassword.setError(getString(R.string.error_password_march));

            } else {
                registerPost();
            }


        } else {
            if( isStringNullOrWhiteSpace(txtFirstname.getText().toString()) ) {
                txtFirstname.setError(getString(R.string.error_field_required));
            }
            if( isStringNullOrWhiteSpace(txtLastname.getText().toString()) ) {
                txtLastname.setError(getString(R.string.error_field_required));
            }
            if( isStringNullOrWhiteSpace(txtPassword.getText().toString()) ) {
                txtPassword.setError(getString(R.string.error_field_required));
            }
            if( isStringNullOrWhiteSpace(txtPasswordConfirm.getText().toString()) ) {
                txtPasswordConfirm.setError(getString(R.string.error_field_required));
            }
        }
    }


    public void registerPost() {

        String url = "https://howcanihelpyou.app/register";

        loader.show();

        JSONObject jsonBody = new JSONObject();

        try {
            String hash = new Hasher().hash(txtPassword.getText().toString(), Hasher.MD5).toUpperCase();
            jsonBody.put("code", txtCode.getText().toString().toUpperCase() );
            String token = FirebaseInstanceId.getInstance().getToken();

            jsonBody.put("firstname", txtFirstname.getText().toString() );
            jsonBody.put("lastname", txtLastname.getText().toString() );
            jsonBody.put("password", hash );
            jsonBody.put("language", "en" );
            jsonBody.put("token", token );

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loader.dismiss();
                System.out.print(response);
                try {

                    if (response.getBoolean("success") ) {

                        regiterDialog.dismiss();
                        validateDialog.dismiss();

                        SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                                "com.inearth.howcanihelpyou", MainActivity.MODE_PRIVATE);
                        prefs.edit().putString("session", response.getString("uuid")).apply();

                        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("session",response.getString("uuid"));
                        editor.commit();
                        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                        getApplicationContext().startActivity(myIntent);

                    } else {

                        TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
                        txtErrorDialog.setText( response.getString("message") );


                        errorDialog.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loader.dismiss();


            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                return params;
            }

        } ;

        jsonOblect.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(jsonOblect);

    }


    public static boolean isStringNullOrWhiteSpace(String value) {
        if (value == null) {
            return true;
        }

        for (int i = 0; i < value.length(); i++) {
            if (!Character.isWhitespace(value.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() >= 6;
    }

    @Override
    public void onBackPressed() {

    }

}

