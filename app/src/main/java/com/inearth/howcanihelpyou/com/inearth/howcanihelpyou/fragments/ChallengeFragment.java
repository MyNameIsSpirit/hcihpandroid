package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inearth.howcanihelpyou.MainActivity;
import com.inearth.howcanihelpyou.R;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.adapters.ChallengeFragmentAdapter;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.ChallengeInviteModel;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.ChallengesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChallengeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChallengeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChallengeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView textView;

    private ImageView button1;
    private ImageView button2;
    private ImageView button3;
    private ImageView button4;

    private ImageView btnuser;

    private TextView text1;
    private TextView text2;
    private TextView text3;
    private TextView text4;

    private TextView txtCoins;

    RequestQueue MyRequestQueue;

    private OnFragmentInteractionListener mListener;

    public ChallengeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChallengeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChallengeFragment newInstance(String param1, String param2) {
        ChallengeFragment fragment = new ChallengeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_challenge, container, false);
        //textView = view.findViewById(R.id.txt);

        button1 = view.findViewById(R.id.button1);
        button2 = view.findViewById(R.id.button2);
        button3 = view.findViewById(R.id.button3);
        button4 = view.findViewById(R.id.button4);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayChallenge((Integer) v.getTag());
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayChallenge((Integer) v.getTag());
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayChallenge((Integer) v.getTag());
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayChallenge((Integer) v.getTag());
            }
        });

        MyRequestQueue = Volley.newRequestQueue(getContext());

        text1 = view.findViewById(R.id.text1);
        text2 = view.findViewById(R.id.text2);
        text3 = view.findViewById(R.id.text3);
        text4 = view.findViewById(R.id.text4);
        btnuser = view.findViewById(R.id.btnuser);
        txtCoins = view.findViewById(R.id.coins);

        int currentChallenge = 3;
        int challengeWord = getResources().getIdentifier ("challenge","string",((MainActivity)getContext()).getPackageName());
        String word = getString( challengeWord );

        if( getArguments().getBoolean("index0")  ) {
            button1.setVisibility(View.VISIBLE);
            text1.setVisibility(View.VISIBLE);

            int number = getArguments().getInt("c0");

            text1.setText(word + " " + number);
            button1.setTag(number);

            if (number < currentChallenge) {
                if( getArguments().getInt("u0") == 0 ){
                    button1.setBackgroundResource(R.drawable.done0);
                }
                if( getArguments().getInt("u0") == 1 ){
                    button1.setBackgroundResource(R.drawable.done1);
                }
                if( getArguments().getInt("u0") == 2 ){
                    button1.setBackgroundResource(R.drawable.done2);
                }
                if( getArguments().getInt("u0") >= 3 ){
                    button1.setBackgroundResource(R.drawable.done3);
                }
            } else if (number == currentChallenge){
                button1.setBackgroundResource(R.drawable.current);
            } else {
                button1.setBackgroundResource(R.drawable.locked);
                button1.setTag(0);
            }

        } else {
            button1.setTag(0);
            button1.setVisibility(View.GONE);
            text1.setVisibility(View.GONE);
        }


        if( getArguments().getBoolean("index1")  ) {
            button2.setVisibility(View.VISIBLE);
            text2.setVisibility(View.VISIBLE);

            int number = getArguments().getInt("c1");

            text2.setText(word + " "  + number);
            button2.setTag(number);

            if (number < currentChallenge) {
                if( getArguments().getInt("u1") == 0 ){
                    button2.setBackgroundResource(R.drawable.done0);
                }
                if( getArguments().getInt("u1") == 1 ){
                    button2.setBackgroundResource(R.drawable.done1);
                }
                if( getArguments().getInt("u1") == 2 ){
                    button2.setBackgroundResource(R.drawable.done2);
                }
                if( getArguments().getInt("u1") == 3 ){
                    button2.setBackgroundResource(R.drawable.done3);
                }

            } else if (number == currentChallenge){
                button2.setBackgroundResource(R.drawable.current);
            } else {
                button2.setBackgroundResource(R.drawable.locked);
                button2.setTag(0);
            }

        } else {
            button2.setTag(0);
            button2.setVisibility(View.GONE);
            text2.setVisibility(View.GONE);
        }


        if( getArguments().getBoolean("index2")  ) {
            button3.setVisibility(View.VISIBLE);
            text3.setVisibility(View.VISIBLE);

            int number = getArguments().getInt("c2");

            text3.setText(word + " "  + number);
            button3.setTag(number);

            if (number < currentChallenge) {
                if( getArguments().getInt("u2") == 0 ){
                    button3.setBackgroundResource(R.drawable.done0);
                }
                if( getArguments().getInt("u2") == 1 ){
                    button3.setBackgroundResource(R.drawable.done1);
                }
                if( getArguments().getInt("u2") == 2 ){
                    button3.setBackgroundResource(R.drawable.done2);
                }
                if( getArguments().getInt("u2") == 3 ){
                    button3.setBackgroundResource(R.drawable.done3);
                }

            } else if (number == currentChallenge){
                button3.setBackgroundResource(R.drawable.current);
            } else {
                button3.setBackgroundResource(R.drawable.locked);
                button3.setTag(0);
            }

        } else {
            button3.setTag(0);
            button3.setVisibility(View.GONE);
            text3.setVisibility(View.GONE);
        }


        if( getArguments().getBoolean("index3")  ) {
            button4.setVisibility(View.VISIBLE);
            text4.setVisibility(View.VISIBLE);

            int number = getArguments().getInt("c3");

            text4.setText(word + " "  + number);
            button4.setTag(number);

            if (number < currentChallenge) {

                if( getArguments().getInt("u3") == 0 ){
                    button4.setBackgroundResource(R.drawable.done0);
                }
                if( getArguments().getInt("u3") == 1 ){
                    button4.setBackgroundResource(R.drawable.done1);
                }
                if( getArguments().getInt("u3") == 2 ){
                    button4.setBackgroundResource(R.drawable.done2);
                }
                if( getArguments().getInt("u3") == 3 ){
                    button4.setBackgroundResource(R.drawable.done3);
                }

            } else if (number == currentChallenge){
                button4.setBackgroundResource(R.drawable.current);
            } else {
                button4.setBackgroundResource(R.drawable.locked);
                button4.setTag(0);
            }

        } else {
            button4.setTag(0);
            button4.setVisibility(View.GONE);
            text4.setVisibility(View.GONE);
        }

        btnuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = ((MainActivity) getActivity()).fragmentManager.beginTransaction();
                ProfileFragment llf = new ProfileFragment();

                Bundle args = new Bundle();
                llf.setArguments(args);

                ft.replace(R.id.fragment_container, llf);
                ft.commit();

                ((MainActivity) getActivity()).currentPage = "ProfileFragment";
            }
        });

        getMe();

        return view;
    }


    private void displayChallenge(int number){

        if (number > 0) {
            FragmentTransaction ft = ((MainActivity) getActivity()).fragmentManager.beginTransaction();
            ShowChallengeFragment llf = new ShowChallengeFragment();

            Bundle args = new Bundle();
            args.putInt("idChallenge", number);
            llf.setArguments(args);

            ft.replace(R.id.fragment_container, llf);
            ft.commit();

            ((MainActivity) getActivity()).currentPage = "ShowCallengeFragment";
        }
    }
    

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void getMe() {

        String url = "https://howcanihelpyou.app/me";
        StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);

                LinkedList<ChallengesModel> model = new LinkedList<ChallengesModel>();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if( jsonObject.getBoolean("success") ) {
                        JSONObject jsonUser = jsonObject.getJSONObject("user");

                        txtCoins.setText(jsonUser.getInt("coins") + "" );
//                        int id = 0;
//                        String email = "";
//                        String name = "";
//                        int inChallange = 0;
//
//                        JSONArray jsonArray = jsonObject.getJSONArray("challenges");
//                        for(int i = 0; i < jsonArray.length(); i++) {
//                            JSONObject subKey = jsonArray.getJSONObject(i);
//                            id = subKey.getInt("id");
//                            ChallengesModel submodel = new ChallengesModel();
//                            submodel.IdChallenge = id;
//                            model.add(submodel);
//                        }
//
//                        JSONArray jsonUsers = jsonObject.getJSONArray("users");
//                        for(int j = 0; j< jsonUsers.length(); j++) {
//                            JSONObject subKey = jsonUsers.getJSONObject(j);
//                            name = subKey.getString("name");
//                            email = subKey.getString("email");
//                            inChallange = subKey.getInt("inChallange");
//
//                            for(int i = 0; i < model.size(); i++) {
//                                if( model.get(i).IdChallenge ==  inChallange ){
//                                    model.get(i).Invites.add(new ChallengeInviteModel(name, email));
//                                }
//                            }
//                        }


                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        };

        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
