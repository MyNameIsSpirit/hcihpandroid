package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models;

public class ChallengeInviteModel {
    public String Name;
    public String Email;

    public ChallengeInviteModel(String name, String email){
        Name = name;
        Email = email;
    }
}
