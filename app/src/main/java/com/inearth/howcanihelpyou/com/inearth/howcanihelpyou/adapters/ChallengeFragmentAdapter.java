package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments.ChallengeFragment;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.ChallengesModel;

import java.util.LinkedList;

public class ChallengeFragmentAdapter extends FragmentStatePagerAdapter {


    LinkedList<ChallengesModel> challenges = new LinkedList<ChallengesModel>();

    public ChallengeFragmentAdapter(FragmentManager fm, LinkedList<ChallengesModel> model) {
        super(fm);
        challenges = model;
    }

    @Override
    public Fragment getItem(int position) {
        ChallengeFragment challengeFragment = new ChallengeFragment();
        Bundle bundle = new Bundle();
        //position = position + 1;


        int currentIndex = 4 * position;

        //bundle.putString("message", "Viewing position: " + position);
        bundle.putBoolean("index0", false);
        bundle.putBoolean("index1", false);
        bundle.putBoolean("index2", false);
        bundle.putBoolean("index3", false);


        if(currentIndex < challenges.size() ){
            bundle.putBoolean("index0", true);
            bundle.putInt("c0", challenges.get(currentIndex).IdChallenge );
            bundle.putInt("u0", challenges.get(currentIndex).Invites.size()  );
        }

        if( (currentIndex + 1) < challenges.size()){
            bundle.putBoolean("index1", true);
            bundle.putInt("c1", challenges.get(currentIndex + 1).IdChallenge );
            bundle.putInt("u1", challenges.get(currentIndex + 1).Invites.size()  );
        }

        if( (currentIndex + 2) < challenges.size()){
            bundle.putBoolean("index2", true);
            bundle.putInt("c2", challenges.get(currentIndex + 2).IdChallenge );
            bundle.putInt("u2", challenges.get(currentIndex + 2).Invites.size()  );
        }

        if( (currentIndex + 3) < challenges.size()){
            bundle.putBoolean("index3", true);
            bundle.putInt("c3", challenges.get(currentIndex + 3).IdChallenge );
            bundle.putInt("u3", challenges.get(currentIndex + 3).Invites.size()  );
        }


        challengeFragment.setArguments(bundle);

        return challengeFragment;
    }

    @Override
    public int getCount() {
        double total = (double) challenges.size();
        double t = Math.ceil(total / 4.0);

        return (int) t;
    }
}
