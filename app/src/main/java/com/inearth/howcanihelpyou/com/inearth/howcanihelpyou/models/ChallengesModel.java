package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models;

import java.util.LinkedList;

public class ChallengesModel {

    public ChallengesModel(){
        Invites = new LinkedList<ChallengeInviteModel>();
    }

    public int IdChallenge;
    public LinkedList<ChallengeInviteModel> Invites;

}
