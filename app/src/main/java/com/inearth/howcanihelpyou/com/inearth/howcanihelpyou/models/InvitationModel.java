package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models;

import java.util.Date;

public class InvitationModel {
    public String email;
    public String name;
    public String expireDate;
    public Boolean verified;

    public InvitationModel(String email, String name, String expireDate, Boolean verified){
        this.email = email;
        this.name = name;
        this.expireDate = expireDate;
        this.verified =  verified;
    }
}
