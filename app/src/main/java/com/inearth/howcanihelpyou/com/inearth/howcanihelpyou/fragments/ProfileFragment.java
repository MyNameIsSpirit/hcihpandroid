package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.inearth.howcanihelpyou.MainActivity;
import com.inearth.howcanihelpyou.R;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.InvitationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    RequestQueue MyRequestQueue;
    TextView txtName;
    TextView slot2text;
    TextView slot1text;
    TextView slot3text;

    Button btnDone;

    public ProfileFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyRequestQueue = Volley.newRequestQueue((MainActivity) getActivity());

        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_profile, container, false);
        txtName = view.findViewById(R.id.name);
        slot3text = view.findViewById(R.id.slot3text);
        slot2text = view.findViewById(R.id.slot2text);
        slot1text = view.findViewById(R.id.slot1text);
        btnDone = view.findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = ((MainActivity) getActivity()).fragmentManager.beginTransaction();

                PagerFragment llf = new PagerFragment();
                ft.replace(R.id.fragment_container, llf);
                ft.commit();
                ((MainActivity) getActivity()).currentPage = "PagerFragment";
            }
        });

        gerProfile();

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void gerProfile() {
        String url = "https://howcanihelpyou.app/me";
        JSONObject jsonBody = new JSONObject();

        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.GET, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                try {
                    System.out.println(response);
                    if( response.getBoolean("success") ) {
                        JSONObject jsonUser = response.getJSONObject("user");
                        txtName.setText( jsonUser.getString("name") );
                        slot3text.setText(response.getInt("stars") + "" );
                        slot2text.setText(response.getInt("stars") + "" );
                        slot1text.setText(jsonUser.getInt("currentChallenge") + "" );

//                        question.setText( response.getString("question_en") );
//                        challengenumber.setText( "CHALLENGE " + idChallenge );
//
//                        JSONArray jsonUsers = response.getJSONArray("invites");
//                        for (int i = 0; i < jsonUsers.length(); i++) {
//                            JSONObject subKey = jsonUsers.getJSONObject(i);
//                            invitations.add(new InvitationModel(  subKey.getString("email"), subKey.getString("name"), subKey.getString("expireDate"), subKey.getBoolean("verified") ));
//                        }
                    } else {
                        //return;
                    }

                } catch (JSONException e) {
                    //hideLoader();
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                //loader.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(((MainActivity) getActivity()).getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        };

        jsonOblect.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(jsonOblect);
    }
}
