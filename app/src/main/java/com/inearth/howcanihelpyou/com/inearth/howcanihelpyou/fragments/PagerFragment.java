package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.inearth.howcanihelpyou.MainActivity;
import com.inearth.howcanihelpyou.R;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.adapters.ChallengeFragmentAdapter;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.ChallengeInviteModel;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.ChallengesModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PagerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PagerFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    RequestQueue MyRequestQueue;
    private ViewPager viewPager;
    private ChallengeFragmentAdapter challengeFragmentAdapter;
    private long mRequestStartTime;

    public PagerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PagerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PagerFragment newInstance(String param1, String param2) {
        PagerFragment fragment = new PagerFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_pager, container, false);

        viewPager = view.findViewById(R.id.pager);
        MyRequestQueue = Volley.newRequestQueue(getContext());
        getChalleges();


        return view;
    }

    private void getChalleges() {
        mRequestStartTime = System.currentTimeMillis();

        String url = "https://howcanihelpyou.app/challenges";
        StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                long totalRequestTime = System.currentTimeMillis() - mRequestStartTime;

                LinkedList<ChallengesModel> model = new LinkedList<ChallengesModel>();

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    if( jsonObject.getBoolean("success") ){
                        int id = 0;
                        String email = "";
                        String name = "";
                        int inChallange = 0;

                        JSONArray jsonArray = jsonObject.getJSONArray("challenges");
                        for(int i = 0; i < jsonArray.length(); i++) {
                            JSONObject subKey = jsonArray.getJSONObject(i);
                            id = subKey.getInt("id");
                            ChallengesModel submodel = new ChallengesModel();
                            submodel.IdChallenge = id;
                            model.add(submodel);
                        }

                        JSONArray jsonUsers = jsonObject.getJSONArray("users");
                        for(int j = 0; j< jsonUsers.length(); j++) {
                            JSONObject subKey = jsonUsers.getJSONObject(j);
                            name = subKey.getString("name");
                            email = subKey.getString("email");
                            inChallange = subKey.getInt("inChallange");

                            for(int i = 0; i < model.size(); i++) {
                                if( model.get(i).IdChallenge ==  inChallange ){
                                    model.get(i).Invites.add(new ChallengeInviteModel(name, email));
                                }
                            }
                        }

                        long finalTime = System.currentTimeMillis() - mRequestStartTime;

                        challengeFragmentAdapter = new ChallengeFragmentAdapter(((MainActivity) getActivity()).fragmentManager, model);
                        viewPager.setAdapter(challengeFragmentAdapter);

                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }


            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        };

        MyStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(MyStringRequest);
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
