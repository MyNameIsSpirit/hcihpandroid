package com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.himanshurawat.hasher.Hasher;
import com.inearth.howcanihelpyou.MainActivity;
import com.inearth.howcanihelpyou.R;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.ChallengeInviteModel;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.models.InvitationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShowChallengeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShowChallengeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShowChallengeFragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    public int idChallenge;
    RequestQueue MyRequestQueue;
    public TextView challengenumber;
    public TextView question;
    public List<InvitationModel> invitations = new LinkedList<>();

    private ImageView slot1;
    private ImageView slot2;
    private ImageView slot3;

    private TextView slot1text;
    private TextView slot2text;
    private TextView slot3text;

    private TextView subslot1text;
    private TextView subslot2text;
    private TextView subslot3text;

    private Dialog dialog;
    private Dialog errorDialog;

    private Dialog levelDialog;
    private TextView txtMessage;
    private ImageView levelImage;

    private OnFragmentInteractionListener mListener;

    private long mRequestStartTime;

    private Button btnDone;

    public ShowChallengeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShowChallengeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShowChallengeFragment newInstance(String param1, String param2) {
        ShowChallengeFragment fragment = new ShowChallengeFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyRequestQueue = Volley.newRequestQueue((MainActivity) getActivity());

        if (getArguments() != null) {
            idChallenge = getArguments().getInt("idChallenge");
        }
    }


    private void getChallenge() {
        mRequestStartTime = System.currentTimeMillis();
        String url = "https://howcanihelpyou.app/challenge?idChallenge=" + idChallenge;
        JSONObject jsonBody = new JSONObject();


        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.GET, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                long totalRequestTime = System.currentTimeMillis() - mRequestStartTime;

                try {

                    if( response.getBoolean("success") ) {

                        if(Locale.getDefault().getLanguage().equals("en")) {
                            question.setText( response.getString("question_en") );
                        } else {
                            question.setText( response.getString("question_es") );
                        }

                        int challengeWord = getResources().getIdentifier ("challenge","string",((MainActivity)getContext()).getPackageName());
                        String word = getString( challengeWord );
                        challengenumber.setText( word + " " + idChallenge );

                        JSONArray jsonUsers = response.getJSONArray("invites");
                        for (int i = 0; i < jsonUsers.length(); i++) {
                            JSONObject subKey = jsonUsers.getJSONObject(i);
                            invitations.add(new InvitationModel(  subKey.getString("email"), subKey.getString("name"), subKey.getString("expireDate"), subKey.getBoolean("verified") ));
                        }
                        buildInvites();

                        JSONArray jsonNotifications = response.getJSONArray("notifications");
                        if (jsonNotifications != null && jsonNotifications.length() >= 1) {
                            JSONObject subKey = jsonNotifications.getJSONObject(0);
                            if( jsonUsers.length()  == 1) {
                                levelImage.setImageResource(R.drawable.level1);
                            }

                            if( jsonUsers.length()  == 2) {
                                levelImage.setImageResource(R.drawable.level2);
                            }

                            if( jsonUsers.length()  >= 3) {
                                levelImage.setImageResource(R.drawable.level3);
                            }

                            txtMessage.setText( subKey.getString("message_en") );
                            levelDialog.show();
                        }

                    } else {
                        //return;
                    }
                    long finalTime = System.currentTimeMillis() - mRequestStartTime;
                } catch (JSONException e) {
                    //hideLoader();
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                //loader.dismiss();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(((MainActivity) getActivity()).getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        };

        jsonOblect.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        MyRequestQueue.add(jsonOblect);
    }


    public void buildInvites(){
        int extra = 3 - invitations.size();

        if( invitations.size() == 0) {
            slot1.setBackground(ContextCompat.getDrawable( ((MainActivity) getActivity()).getApplicationContext() , R.drawable.invite));
            slot2.setBackground(ContextCompat.getDrawable( ((MainActivity) getActivity()).getApplicationContext() , R.drawable.invite));
            slot3.setBackground(ContextCompat.getDrawable( ((MainActivity) getActivity()).getApplicationContext() , R.drawable.invite));

            slot1text.setText( R.string.invite );
            slot2text.setText( R.string.invite );
            slot3text.setText( "Invite" );

            slot1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invite();
                }
            });
            slot2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invite();
                }
            });
            slot3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invite();
                }
            });

        } else if( invitations.size() == 1) {

            if (invitations.get(0).verified == true) {
                slot1.setImageResource(R.drawable.verified);
                slot1text.setText(invitations.get(0).name );
                subslot1text.setText(R.string.verified);
            } else {
                String d = StringToDate(invitations.get(0).expireDate);
                slot1.setImageResource(R.drawable.waiting);
                slot1text.setText( d );
                subslot1text.setText( invitations.get(0).email);
            }

            slot2.setImageResource(R.drawable.invite);
            slot3.setImageResource(R.drawable.invite);

            slot2text.setText(R.string.invite);
            slot3text.setText(R.string.invite);

            slot2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invite();
                }
            });
            slot3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invite();
                }
            });

        } else if( invitations.size() == 2) {

            if (invitations.get(0).verified == true) {
                slot1.setImageResource(R.drawable.verified);
                slot1text.setText(invitations.get(0).name );
                subslot1text.setText(R.string.verified);
            } else {
                String d = StringToDate(invitations.get(0).expireDate);
                slot1.setImageResource(R.drawable.waiting);
                slot1text.setText( d );
                subslot1text.setText( invitations.get(0).email);
            }

            if (invitations.get(1).verified == true) {
                slot2.setImageResource(R.drawable.verified);
                slot2text.setText(invitations.get(1).name );
                subslot2text.setText(R.string.verified);
            } else {
                String d = StringToDate(invitations.get(1).expireDate);
                slot2.setImageResource(R.drawable.waiting);
                slot2text.setText( d );
                subslot2text.setText( invitations.get(1).email);
            }

            slot3.setImageResource(R.drawable.invite);
            slot3text.setText(R.string.invite);

            slot3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invite();
                }
            });

        } else if( invitations.size() == 3) {

            if (invitations.get(0).verified == true) {
                slot1.setImageResource(R.drawable.verified);
                slot1text.setText(invitations.get(0).name );
                subslot1text.setText(R.string.verified);
            } else {
                String d = StringToDate(invitations.get(0).expireDate);
                slot1.setImageResource(R.drawable.waiting);
                slot1text.setText( d );
                subslot1text.setText( invitations.get(0).email);
            }

            if (invitations.get(1).verified == true) {
                slot2.setImageResource(R.drawable.verified);
                slot2text.setText(invitations.get(1).name );
                subslot2text.setText( R.string.verified);
            } else {
                String d = StringToDate(invitations.get(1).expireDate);
                slot2.setImageResource(R.drawable.waiting);
                slot2text.setText( d );
                subslot2text.setText( invitations.get(1).email);
            }

            if (invitations.get(2).verified == true) {
                slot3.setImageResource(R.drawable.verified);
                slot3text.setText(invitations.get(2).name );
                subslot3text.setText(R.string.verified);
            } else {
                String d = StringToDate(invitations.get(2).expireDate);
                slot3.setImageResource(R.drawable.waiting);
                slot3text.setText( d );
                subslot3text.setText( invitations.get(2).email);
            }

        }

    }

    public String StringToDate(String stringDate) {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(stringDate.replace("T", " ")  );
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        String formattedDate = df.format(date);


        Date today = new Date();

        long diff = date.getTime() - today.getTime();


        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);

        return diffHours + "H " + diffMinutes + "m";
    }


    public void invite() {
        dialog.setContentView(R.layout.custom);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        Button dialogButton = dialog.findViewById(R.id.btninvite);
        final EditText inviteEmail = dialog.findViewById(R.id.inviteEmail);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if ( !inviteEmail.getText().equals("") ) {

                    boolean match = android.util.Patterns.EMAIL_ADDRESS.matcher(inviteEmail.getText()).matches();

                    if(match) {
                        sendInvite(inviteEmail.getText().toString());
                    } else {
                        /*new AlertDialog.Builder( ((MainActivity) getActivity()) )
                                .setTitle("Error")
                                .setMessage("The email format is not valid")
                                .setPositiveButton(android.R.string.ok, null)
                                .show();*/
                        TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
                        txtErrorDialog.setText( "The email format is not valid" );
                        Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
                        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errorDialog.dismiss();

                            }
                        });
                        errorDialog.show();
                    }

                } else {
                    /*new AlertDialog.Builder(  ((MainActivity) getActivity()) )
                            .setTitle("Error")
                            .setMessage("The email is empty")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();*/
                    TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
                    txtErrorDialog.setText( "The email field is empty" );
                    Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
                    btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            errorDialog.dismiss();

                        }
                    });
                    errorDialog.show();
                }

            }
        });
        dialog.show();
    }



    private void sendInvite(String email){
        String url = "https://howcanihelpyou.app/invite?email=" + email + "&idChallenge=" + idChallenge + "&language=en";
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("email", email);
            jsonBody.put("language", "en");
            jsonBody.put("idChallenge", idChallenge);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println(response);
                try {
                    if( response.getBoolean("success") ){
                        displayConfirm();
                    } else {

                        TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);


                        int idwrongcrenditlas = getResources().getIdentifier (response.getString("message"),"string", ((MainActivity)getContext()).getPackageName() );

                        txtErrorDialog.setText( idwrongcrenditlas );
                        Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
                        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                errorDialog.dismiss();

                            }
                        });
                        errorDialog.show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences((MainActivity) getActivity());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        } ;

        MyRequestQueue.add(jsonOblect);
    }

    private void displayConfirm() {


        dialog.dismiss();



        TextView txtErrorDialog = errorDialog.findViewById(R.id.txtErrorDescription);
        txtErrorDialog.setText( "Your friend has been invited." );
        Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
        btnCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errorDialog.dismiss();

                FragmentTransaction ft =  ((MainActivity) getActivity()).fragmentManager.beginTransaction();
                ShowChallengeFragment llf = new ShowChallengeFragment();

                Bundle args = new Bundle();
                args.putInt("idChallenge", idChallenge);
                llf.setArguments(args);

                ft.replace(R.id.fragment_container, llf);
                ft.commit();

            }
        });
        errorDialog.show();






        //ft.det
        //ft.attach(ft);


        /*AlertDialog.Builder builder = new AlertDialog.Builder((MainActivity) getActivity());

        builder.setTitle("Confirm");
        builder.setMessage("Invitation sent.");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog





            }
        });

        AlertDialog alert = builder.create();
        alert.show();

        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));*/
        //alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAlert));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_show_challenge, container, false);
        challengenumber = view.findViewById(R.id.challengenumber);
        question = view.findViewById(R.id.question);

        slot1 = view.findViewById(R.id.slot1);
        slot2 = view.findViewById(R.id.slot2);
        slot3 = view.findViewById(R.id.slot3);

        slot1text = view.findViewById(R.id.slot1text);
        slot2text = view.findViewById(R.id.slot2text);
        slot3text = view.findViewById(R.id.slot3text);

        subslot1text = view.findViewById(R.id.subslot1text);
        subslot2text = view.findViewById(R.id.subslot2text);
        subslot3text = view.findViewById(R.id.subslot3text);

        dialog = new Dialog(  ((MainActivity) getActivity())  );
        errorDialog = new Dialog(  ((MainActivity) getActivity())  );
        levelDialog = new Dialog(  ((MainActivity) getActivity())  );

        errorDialog.setContentView(R.layout.error);
        errorDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        levelDialog.setContentView(R.layout.star_layout);
        levelDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        txtMessage = levelDialog.findViewById(R.id.txtMessage);
        levelImage = levelDialog.findViewById(R.id.levelImage);

        btnDone = view.findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = ((MainActivity) getActivity()).fragmentManager.beginTransaction();

                PagerFragment llf = new PagerFragment();
                ft.replace(R.id.fragment_container, llf);
                ft.commit();
                ((MainActivity) getActivity()).currentPage = "PagerFragment";
            }
        });

        getChallenge();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public boolean onBackPressed() {

        // -- your code --

        // return true if you want to consume back-pressed event
        return false;
    }

}
