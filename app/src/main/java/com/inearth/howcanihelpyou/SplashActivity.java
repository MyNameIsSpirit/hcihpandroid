package com.inearth.howcanihelpyou;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments.ShowChallengeFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    Context context;
    RequestQueue MyRequestQueue;
    BroadcastReceiver mBroadcastReceiver;

    public static final String RECEIVER_INTENT = "RECEIVER_INTENT";
    public static final String RECEIVER_MESSAGE = "RECEIVER_MESSAGE";
    public android.support.v4.app.FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        getSupportActionBar().hide();
        setContentView(R.layout.activity_splash);

        MyRequestQueue = Volley.newRequestQueue(this);
        context = this;


        fragmentManager = getSupportFragmentManager();

        mBroadcastReceiver= new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra(RECEIVER_MESSAGE);
                // call any method you want here
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ShowChallengeFragment llf = new ShowChallengeFragment();
                llf.idChallenge = 1;
                ft.replace(R.id.fragment_container, llf);
                ft.commit();
                //currentPage = "PagerFragment";
            }
        };

        Intent i = getIntent();

        Bundle extras = i.getExtras();

        if(extras != null) {
            String push = extras.getString("push");
            if (push != null) {
                Integer idChallenge = Integer.parseInt( extras.getString("IdChallenge") );

                Intent myIntent = new Intent(context, MainActivity.class);
                myIntent.putExtra("IdChallenge", idChallenge);

                context.startActivity(myIntent);
            } else {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
                String session = sp.getString("session", "");

                if(!session.equalsIgnoreCase("")){
                    isvalidSession();
                } else {
                    Intent myIntent = new Intent(context, LoginActivity.class);
                    context.startActivity(myIntent);
                }
            }
        } else {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            String session = sp.getString("session", "");

            if(!session.equalsIgnoreCase("")){
                isvalidSession();
            } else {
                Intent myIntent = new Intent(context, LoginActivity.class);
                context.startActivity(myIntent);
            }
        }




    }


    private void isvalidSession() {

        String url = "https://howcanihelpyou.app/valid";
        JSONObject jsonBody = new JSONObject();

        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    //hideLoader();
                    if( response.getBoolean("success") ){
                        Intent myIntent = new Intent(context, MainActivity.class);
                        context.startActivity(myIntent);
                    } else {
                        Intent myIntent = new Intent(context, LoginActivity.class);
                        context.startActivity(myIntent);
                    }
                } catch (JSONException e) {
                    Intent myIntent = new Intent(context, LoginActivity.class);
                    context.startActivity(myIntent);
                }

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                Intent myIntent = new Intent(context, LoginActivity.class);
                context.startActivity(myIntent);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        } ;

        MyRequestQueue.add(jsonOblect);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mBroadcastReceiver),
                new IntentFilter(RECEIVER_INTENT)
        );
    }


    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        super.onStop();
    }


}
