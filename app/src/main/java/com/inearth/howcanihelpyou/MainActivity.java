package com.inearth.howcanihelpyou;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.blikoon.qrcodescanner.QrCodeActivity;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments.ChallengeFragment;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments.PagerFragment;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments.ProfileFragment;
import com.inearth.howcanihelpyou.com.inearth.howcanihelpyou.fragments.ShowChallengeFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ChallengeFragment.OnFragmentInteractionListener,
        ShowChallengeFragment.OnFragmentInteractionListener, PagerFragment.OnFragmentInteractionListener,
        ProfileFragment.OnFragmentInteractionListener {

    private TextView txtCoins;
    private MainActivity context;
    private TextView lblQuestion;
    private ImageButton fab;
    private ImageButton fabFB;
    private ImageButton fabTw;

    private Button btnDone;
    private Editable inviteEmailAddress;
    EditText inviteEmail;
    private Dialog dialog;
    private Dialog loader;
    private Button btnShare;

    RequestQueue MyRequestQueue;
    private Dialog errorDialog;

    private static final int REQUEST_CODE_QR_SCAN = 101;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";

    public android.support.v4.app.FragmentManager fragmentManager;
    BroadcastReceiver mBroadcastReceiver;

    public static final String RECEIVER_INTENT = "RECEIVER_INTENT";
    public static final String RECEIVER_MESSAGE = "RECEIVER_MESSAGE";

    public String currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("~> ~> ~> ~> ~> ~> ~> ~> ~> ~> ~> ~>  Main has been recently executed");

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        MyRequestQueue = Volley.newRequestQueue(this);
        setSupportActionBar(toolbar);
        context = this;
        fragmentManager = getSupportFragmentManager();


        Intent i = getIntent();
        Bundle extras = i.getExtras();

        if( extras.get("IdChallenge") == null ) {

            FragmentTransaction ft = fragmentManager.beginTransaction();
            PagerFragment llf = new PagerFragment();
            ft.replace(R.id.fragment_container, llf);
            ft.commit();
            currentPage = "PagerFragment";
        } else {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ShowChallengeFragment llf = new ShowChallengeFragment();

            Bundle args = new Bundle();
            args.putInt("idChallenge", extras.getInt("IdChallenge"));
            llf.setArguments(args);

            ft.replace(R.id.fragment_container, llf);
            ft.commit();
            currentPage = "ShowCallengeFragment";
        }



        errorDialog = new Dialog( this );
        errorDialog.setContentView(R.layout.logoutlayout);
        errorDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


//        loader.findViewById(R.id.loadicon).startAnimation(rotateAnimation);
//
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(context,  Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (getFromPref(context, ALLOW_KEY)) {
                        showSettingsAlert();
                    } else if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        // Should we show an explanation?
                        if (ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.CAMERA)) {
                            showAlert();
                        } else {
                            // No explanation needed, we can request the permission.
                            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    }
                } else {
                    openCamera();
                }

                ActivityCompat.requestPermissions(context,  new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
            }
        });
//
//
//
//
//        fabFB.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//                String currentEmail = sp.getString("session", "");
//
//
//                FacebookSdk.sdkInitialize(context);
//                CallbackManager callbackManager = CallbackManager.Factory.create();
//
//                FacebookSdk.sdkInitialize(getApplicationContext());
//                callbackManager = CallbackManager.Factory.create();
//                LoginManager loginManager = LoginManager.getInstance();
//                shareDialog = new ShareDialog(context);
//                linkContent = new ShareLinkContent.Builder()
//                        .setContentUrl(Uri.parse("https://howcanihelpyou.app/helpme?source=" + currentEmail)).build();
//                shareDialog.show(linkContent);
//
//
//                shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
//                    @Override
//                    public void onSuccess(Sharer.Result result) {
//
//                    }
//
//                    @Override
//                    public void onCancel() {
//
//                    }
//
//
//                    @Override
//                    public void onError(FacebookException error) {
//                        if (error != null && error.getMessage().equals("null")) {
//                            // Don't use the app for sharing in case of null-error
//                            //shareDialog.show(linkContent, ShareDialog.Mode.WEB);
//                        }
//                    }
//
//
//                });
//            }
//
//
//            public void onError(FacebookException error) {
//                if (error != null && error.getMessage().equals("null")) {
//                    // Don't use the app for sharing in case of null-error
//                    shareDialog.show(linkContent, ShareDialog.Mode.WEB);
//                }
//            }
//        });
//
//        fabTw.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//                String currentEmail = sp.getString("session", "");
//
//                String tweetUrl = "https://twitter.com/intent/tweet?text=Help me to improve the world, together we can achive it! %23HowCanIHelpYou %23PrayItForward                          &url="  + "https://howcanihelpyou.app/helpme?source=" + currentEmail;
//                Uri uri = Uri.parse(tweetUrl);
//                startActivity(new Intent(Intent.ACTION_VIEW, uri));
//            }
//        });
//
//
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mBroadcastReceiver= new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String message = intent.getStringExtra(RECEIVER_MESSAGE);
                // call any method you want here
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ShowChallengeFragment llf = new ShowChallengeFragment();
                llf.idChallenge = 1;
                ft.replace(R.id.fragment_container, llf);
                ft.commit();
                //currentPage = "PagerFragment";
            }
        };
//
//
//
//        btnDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//                startActivity(getIntent());
//            }
//        });
//
//        btnShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //finish();
//                //startActivity(getIntent());
//                share();
//            }
//        });
//

//
        //CookieHandler.setDefault(new CookieManager());
//        getMe();
//        getMyInvites();
//        getChallenges();
    }


    /*public void onError(FacebookException error) {
        if (error != null && error.getMessage().equals("null")) {
            // Don't use the app for sharing in case of null-error
            shareDialog.show(linkContent, ShareDialog.Mode.WEB);
        }
    }*/

















    private void getMe(){
        String url = "https://howcanihelpyou.app/me";
        StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = new JSONArray(response);
                    //txtCoins.setText(jsonObject[0]["coins"].toString()); ;
                    for(int i=0; i < jsonArray.length(); i++) {
                        JSONObject jsonobject = jsonArray.getJSONObject(i);
                        String coins = jsonobject.getString("coins");
                        txtCoins.setText(coins);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //mEmailSignInButton.setVisibility(View.VISIBLE);
                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        };

        //MyRequestQueue.add(MyStringRequest);
    }





    private void displayConfirm() {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle("Confirm");
        builder.setMessage("Invitation sent.");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
                finish();
                startActivity(getIntent());
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
        //alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAlert));
    }


    private void sendInvite(){
        String url = "https://howcanihelpyou.app/sendInvitation?email=" + inviteEmailAddress.toString();
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("email", String.valueOf(inviteEmailAddress));
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        loader.show();

        //StringRequest MyStringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.GET, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loader.dismiss();
                try {
                    if( response.getBoolean("success") ){
                        displayConfirm();

                    } else {

                        new AlertDialog.Builder(context)
                                .setTitle("Error")
                                .setMessage(response.getString("message"))
                                .setPositiveButton(android.R.string.ok, null)
                                .show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                loader.dismiss();
                //This code is executed if there is an error.
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        } ;

        //MyRequestQueue.add(jsonOblect);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode != Activity.RESULT_OK)
        {
            //Log.d(LOGTAG,"COULD NOT GET A GOOD RESULT.");
            if(data==null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.error_decoding_image");
            if( result!=null)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Scan Error");
                alertDialog.setMessage("QR Code could not be scanned");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
            return;

        }
        if(requestCode == REQUEST_CODE_QR_SCAN) {
            if (data == null)
                return;
            //Getting the passed result
            String result = data.getStringExtra("com.blikoon.qrcodescanner.got_qr_scan_relult");
            //Log.d(LOGTAG,"Have scan result in your app activity :"+ result);
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

            if (result.toLowerCase().matches("(https://howcanihelpyou.app/).*")){

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://howcanihelpyou.app/codes?code=friendship&installed=true"));
                startActivity(browserIntent);

            } else {
                new AlertDialog.Builder(context)
                        .setTitle("")
                        .setMessage("")
                        .setPositiveButton(android.R.string.ok, null)
                        .show();

            }

        }
    }

    private void share(){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);

        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Hey! checkout https://HowCanIHelpYou.app and help me to make a better world. #HowCanIHelpYou #PayItForward");
        startActivity(Intent.createChooser(intent, "Share"));
    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }




    public static void saveToPreferences(Context context, String key,
                                         Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences
                (CAMERA_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }

    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences
                (CAMERA_PREF, Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(MainActivity.this,
                                new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_REQUEST_CAMERA);

                    }
                });
        alertDialog.show();
    }

    private void showSettingsAlert() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(MainActivity.this);

                    }
                });
        alertDialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                for (int i = 0, len = permissions.length; i < len; i++) {
                    String permission = permissions[i];
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale =
                                ActivityCompat.shouldShowRequestPermissionRationale
                                        (this, permission);
                        if (showRationale) {
                            showAlert();
                        } else if (!showRationale) {
                            // user denied flagging NEVER ASK AGAIN
                            // you can either enable some fall back,
                            // disable features of your app
                            // or open another dialog explaining
                            // again the permission and directing to
                            // the app setting
                            saveToPreferences(MainActivity.this, ALLOW_KEY, true);
                        }
                    }
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request

        }
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private void openCamera() {
        //Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        //startActivity(intent);
        Intent i = new Intent(MainActivity.this,QrCodeActivity.class);
        startActivityForResult( i,REQUEST_CODE_QR_SCAN);
    }


    private void logout(){
        String url = "https://howcanihelpyou.app/logoutmobile";
        JSONObject jsonBody = new JSONObject();

        JsonObjectRequest jsonOblect = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Intent myIntent = new Intent(context, LoginActivity.class);
                context.startActivity(myIntent);

            }

        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                //This code is executed if there is an error.
                Intent myIntent = new Intent(context, LoginActivity.class);
                context.startActivity(myIntent);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String,String> params = new HashMap<String, String>();

                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String session = sp.getString("session", "");
                params.put("Cookie", "__session__=" + session);

                return params;
            }

        } ;

        MyRequestQueue.add(jsonOblect);
    }







    /*@Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main,menu);
        //return super.onCreateOptionsMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.logout) {

            Button btnCloseDialog = errorDialog.findViewById(R.id.btnClose);
            btnCloseDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //errorDialog.dismiss();
                    logout();
                }
            });
            errorDialog.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if(currentPage == "PagerFragment"){

        } else if (currentPage == "ShowCallengeFragment" ){
            PagerFragment llf = new PagerFragment();
            ft.replace(R.id.fragment_container, llf);
            ft.commit();
            currentPage = "ShowCallengeFragment";

        } else if (currentPage == "ProfileFragment" ){
            PagerFragment llf = new PagerFragment();
            ft.replace(R.id.fragment_container, llf);
            ft.commit();
            currentPage = "PagerFragment";

        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mBroadcastReceiver),
                new IntentFilter(RECEIVER_INTENT)
        );
    }


    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        super.onStop();
    }

}
